#include <mbed.h>
#include <equeue/equeue_platform.h>
#include <events/mbed_events.h>
#include <mbed_error.h>
#include <string>
#include <SerialEvent.h>
#include <rtos.h>
#include <light.h>
#include <helperFunctions.h>
#include <servoInterface.h>

using namespace std;

// Normal Serial connection to a pc and the logger Nano
Serial pc(USBTX, USBRX, 115200);

// Ticker to end the game after 100 seconds
Ticker gameTimeout;

// The activation System detection
DigitalIn activationSystem(PA_12, PullUp);

// Should debug information be printed?
bool debugOn = false;
// Is the game still running?
bool gameRunning = true;

/**
 * This method saves the log on the Nano, 
 * stops all mechanical actions,
 * disabled the vacuum and releases it,
 * and then kills all other programs running
*/ 

void endGame() {
  gameRunning = false;
  pc.printf("!END LOGGING!\n");
  interruptDrive = 1;
  pumpRelais = 1;
  vacuumRelease = 0;
  armZEnable = 1;
  exit(0);
}

void informationPrint() {
  pc.printf("Wheel %f mm\n", wheelDiameter);
  wait_ms(50);
  pc.printf("Outer Dist %f mm\n", outerWheelDistance*10);
  wait_ms(50);
  pc.printf("Mid Dist %f mm\n\n", midWheelDistance);
  wait_ms(50);
  pc.printf("Steps/rev %f\n\n", stepsPerRev);
  wait_ms(50);
  pc.printf("Dist/Step %f mm\n", distancePerStep);
  wait_ms(50);
  pc.printf("Angle/Step %f°\n\n", anglePerStep);
  wait_ms(50);
  pc.printf("Time per step %f µs\n\n", tPerStep);
  wait_ms(50);
  pc.printf("Stop distance %f mm\n\n", stopDist);
  wait_ms(50);
}

// Main debug program
// All code here will be executed
int mainDebug() {
  setRed();
  pc.baud(115200);
  pc.printf("NUCLEO online!\n");
  wait(5);
  pc.printf("NUCLEO is starting!\n");
  pc.printf("Compiled at: %s %s\n", __TIME__, __DATE__);

  initAll();
  informationPrint();
  sensorReader.start(readSensorsCont);

  off();
  wait(1);
  setColor(255, 0, 0);
  wait(1);
  setColor(0, 255, 0);
  wait(1);
  setColor(0, 0, 255);
  wait(1);

  int team = getTeam();
  pc.printf("Team (ID) %d\n", team);
  pc.printf("Team %s\n\n", team == 1 ? "Green/Purple" : (team == 3 ? "ERROR" : "Orange"));
  if (team == 1) {
    for (int i = 0; i < 3; i++) {
      setViolet();
      wait_ms(500);
      setColor(0,0,0);
      wait_ms(500);
    }
  } else if (team == 2) {
    for (int i = 0; i < 3; i++) {
      setOrange();
      wait_ms(500);
      off();
      wait_ms(500);
    }
  }

  while(!activationSystem){
    wait_ms(10);
  }
  pc.printf("STARTED!\n");
  //gameTimeout.attach(&endGame, 100);
  setGreen();

  drive(1000, false, true);
  drive(1000, true, true);
  //activateOthers(team);

  /*collect();
  store(0);

  drive(300, false, false);

  collect();
  store(1);

  wait(3);

  collectStorage(1);
  wait(0.5);
  //moveJointAbsolute(0, 90, 500);
  pushIn(150, false);
  moveJointLVL3Up(true);
  wait(0.5);
  //pushIn(60, true, true);
  pushIn(200, true);
  wait(0.5);
  dropAtom();
  wait(1);

  pushIn(150, true);
  collectStorage(0);
  wait(1);
  //moveJointAbsolute(90, 0, 500);
  pushIn(150, false);
  moveJointLVL3Up(true);
  wait(0.5);
  //pushIn(60, true, true);
  pushIn(200, true);
  wait(0.5);
  dropAtom();
  wait(5);*/

  /*drive(1000, true, false);
  drive(1000, false, false);

  drive(1000, true, true);
  drive(1000, false, true);
  
  turn(360, true, false);  
  turn(360, false, false);

  turn(360, true, true);  
  turn(360, false, true);*/

  endGame();

  /*pc.printf("Mega to the side\n");
  writeMega(10);
  pc.printf("Arm down\n");
  moveArmZ(600, true);
  pc.printf("Pump on\n");
  switchPump(true);
  wait(5);
  pc.printf("Arm up\n");
  moveArmZ(600, false);
  pc.printf("Tilt to the up\n");
  writeTilt(100);*/
  /*switchPump(false);
  pc.printf("Drop Atom\n");
  dropAtom();*/

  /*drive(1000, true, true);
  drive(1000, false, true);*/

  //dropAtom();

  /*wait(5);
  drive(290, true, false);
  drive(290, false, false);
  pc.printf("Now i will turn!\n");
  turn(90, true, false);  
  turn(90, false, false);

  moveArmZ(200, true);
  wait(1);
  moveArmZ(200, false);
  wait(1);

  wait(10);
  switchPump();
  wait(3);

  switchPump();
  wait(4);

  dropAtom();
  wait(5);

  writeMega(10);
  writeTilt(100);
  wait(5);
  writeTilt(0);
  writeMega(100);*/
  while(1) {
    /*activateOthers(i++);
    wait(1);
    driveWithoutAvoidance(850, true);
    wait(5);
    driveWithoutAvoidance(850, false);
    wait(2);*/
    wait(5);

    /*writeMega(10);
    pc.printf("Mega --> 10\n");
    writeTilt(100);
    pc.printf("Tilt --> 100\n");
    wait(5);
    writeTilt(0);
    pc.printf("Tilt --> 0\n");
    writeMega(100);
    pc.printf("Mega --> 100\n");*/
    /*
    switchPump();
    wait(3);

    switchPump();
    wait(4);

    dropAtom();
    wait(5);*/
    /*moveArmZ(1000, true);
    wait(5);
    moveArmZ(1000, false);
    wait(2);*/
  }
}

// This is what the bot does when he is team orange
void gameRoutineOrange() {
  collect();
  store(0);

  drive(300, true, false);

  collect();
  //moveZAxisAbsolute(35);
  //moveJointAbsoluteNoWait(165, 80, 600);
  store(1);

  drive(500, false, false);
  //dropAtom();
  drive(400, false, true);
  drive(100, false, false);
  drive(380, true, true);
  turn(90, true, false);
  drive(300, false, false);
  wait(33);
  drive(1580, true, true);

  /*turn(90, false, true);
  drive(100, false, false);
  drive(100, true, true);
  drive(90, true, true);*/

  pushIn(150, false);
  moveJointLVL3Up(true);
  moveZAxisAbsolute(210);
  wait(0.5);
  pushIn(195, false);

  moveJointAbsolute(currentAngleJointLVL1, -45, 300);
  moveJointAbsolute(currentAngleJointLVL1, 45, 300);
  pushIn(195, false);

  pushIn(150, false);

  collectStorage(1);

  pushIn(150, false);
  moveJointLVL3Up(true);
  moveZAxisAbsolute(210);
  wait(0.5);
  pushIn(195, false);
  wait(0.5);
  dropAtom();
  wait(1);

  pushIn(150, false);

  pushIn(150, false);
  wait(0.5);
  pushIn(195, false);

  moveJointLVL3Up(true);
  moveJointAbsolute(currentAngleJointLVL1, -45, 300);
  moveJointAbsolute(currentAngleJointLVL1, 45, 300);
  pushIn(195, false);

  collectStorage(0);

  pushIn(150, false);
  moveJointLVL3Up(true);
  moveZAxisAbsolute(210);
  wait(0.5);
  pushIn(200, false);
  wait(0.5);
  dropAtom();
  wait(1);
  pushIn(150, false);

  pushIn(150, false);
  wait(0.5);
  pushIn(195, false);

  moveJointLVL3Up(true);
  moveJointAbsolute(currentAngleJointLVL1, -45, 300);
  moveJointAbsolute(currentAngleJointLVL1, 45, 300);

  pushIn(150, false);
  moveZAxisAbsolute(220);
  moveJointLVL3Up(false);

  turn(5, false, false);

  drive(550, true, true);

  switchPump(true);
  moveJointLVL3Up(true);
  pushIn(195, false);
  pushIn(150, false);
  moveJointLVL3Up(false);

  store(1);
}

// This is what the bot does when he is team purple
void gameRoutinePurple() {
  collect();
  store(0);

  drive(300, false, false);

  collect();
  store(1);

  drive(500, true, false);
  drive(400, true, true);
  drive(100, true, false);
  drive(380, false, true);
  turn(90, false, false);
  drive(300, true, false);
  drive(1580, false, true);

  pushIn(150, false);
  moveJointLVL3Up(true);
  moveZAxisAbsolute(210);
  wait(0.5);
  pushIn(195, false);

  moveJointAbsolute(currentAngleJointLVL1, -45, 300);
  moveJointAbsolute(currentAngleJointLVL1, 45, 300);
  pushIn(195, false);

  pushIn(150, false);

  collectStorage(1);

  pushIn(150, false);
  moveJointLVL3Up(true);
  moveZAxisAbsolute(210);
  wait(0.5);
  pushIn(195, false);
  wait(0.5);
  dropAtom();
  wait(1);

  pushIn(150, false);

  pushIn(150, false);
  wait(0.5);
  pushIn(195, false);

  moveJointLVL3Up(true);
  moveJointAbsolute(currentAngleJointLVL1, -45, 300);
  moveJointAbsolute(currentAngleJointLVL1, 45, 300);
  pushIn(195, false);

  collectStorage(0);

  pushIn(150, false);
  moveJointLVL3Up(true);
  moveZAxisAbsolute(210);
  wait(0.5);
  pushIn(200, false);
  wait(0.5);
  dropAtom();
  wait(1);
  pushIn(150, false);

  pushIn(150, false);
  wait(0.5);
  pushIn(195, false);

  moveJointLVL3Up(true);
  moveJointAbsolute(currentAngleJointLVL1, -45, 300);
  moveJointAbsolute(currentAngleJointLVL1, 45, 300);

  pushIn(150, false);
  moveZAxisAbsolute(220);
  moveJointLVL3Up(false);

  turn(5, true, false);

  drive(550, false, true);

  switchPump(true);
  moveJointLVL3Up(true);
  pushIn(195, false);
  pushIn(150, false);
  moveJointLVL3Up(false);

  store(1);
}

// Main program running during a game
// All code here will be executed
int mainGame() {
  // Set the bot to a nice red color to show that it's in init
  setRed();
  pc.printf("NUCLEO online!\n");
  wait(0.5);
  pc.printf("NUCLEO is starting!\n");
  pc.printf("Compiled at: %s %s\n",__TIME__,__DATE__);

  // Init all systems
  initAll();
  informationPrint();

  // Get the team
  int team = getTeam();
  //int team = 1;
  pc.printf("Team (ID) %d\n", team);
  pc.printf("Team %s\n\n", team == 1 ? "Purple" : (team == 3 ? "ERROR" : "Yellow"));

  // Start reading the sensors
  sensorReader.start(readSensorsCont);

  // Set the bot to a nice blue color to show that it's ready
  setBlue();

  // Wait until the bot is activated
  while(!activationSystem){
    wait_ms(10);
  }
  pc.printf("STARTED!\n");
  // Start the 100 second countdown
  gameTimeout.attach(&endGame, 100);

  // Activate all other (of our) elements and send the team-ID
  activateOthers(team);

  // Set the bot to a nice green color to show that it has successfully started
  setGreen();
  
  // Start the right routine according to the team
  if (team == 1) { //purple
    gameRoutinePurple();
  }
  else if (team == 2) {
    gameRoutineOrange();
  }
  else {
    /*  Fallback!!
        This will start an emergency protocol in case the robot does not recognise it's team.
        In this case it will drive a bit until it leaves the box and then stays in it's position.
        All other systems will be activated!
    */
   turn(90, true, false);
   drive(80, true, false);
  }
  
  return 0;

}

// Main program
// All code here will be executed
int main() {
  //mainDebug();
  mainGame();
}
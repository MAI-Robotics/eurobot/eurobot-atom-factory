#ifndef DRIVEDRIVER_H
#define DRIVEDRIVER_H

#include <mbed.h>
#include <sensors.h>
#include <rtos.h>

/**** Some constants ****/
extern Serial pc;                         // Normal Serial connection
Serial driveSerial(PC_12, PD_2, 115200); // Connection to the Nano that is used for driving

/**** Some constants ****/
const double PI = atan(1.0) * 4;        // Math PI (Why not existing in C++?)

const float wheelDiameter = 79.4;       // mm
const float outerWheelDistance = 30.1;  // cm
const float wheelWidth = 19.1;          // mm
const float stepsPerRev = 850;          // Steps that are needed to do one revolution

// Calculate the values for later calculating the amount of steps
const float midWheelDistance = outerWheelDistance * 10 - wheelWidth;
const float distancePerStep = PI * wheelDiameter / stepsPerRev;               //mm
const float anglePerStep = 360 / ((PI * midWheelDistance) / distancePerStep); //°

// Tolerance in mm added to the stop distance
const float tolerance = 100;
// Minimum distance in wich we have to stop if an obstacle is detected
const float stopDist = 100 * distancePerStep + tolerance;

const float tPerStep = 840; //minimal delay between the steps in µs
const float tPerSlowStep = tPerStep * 1;

// Feedback line from the driving Nano
// High if we can send commands
DigitalIn feedback(A3);
// Pull this high to interrupt the driving Nano and stop the bot
DigitalOut interruptDrive(A4);

/**
 * Method to turn the bot
 * @param angle   Angle which the bot has to turn
 * @param dir     Direction in wich the bot will turn true: left | false: right
 * @param useCoA  Wether to use collision avoidance or not
*/
float turn(float angle, bool dir, bool useCoA = true)
{
  int stepsDone = 0;
  int stepsToDo = angle / anglePerStep;
  if (useCoA)
  {
    printf("I will do %d steps with CoA\n", stepsToDo);
    while (!feedback)
    {
      wait_ms(5);
    }

    if (feedback)
    {
      driveSerial.printf("!!t%5d\n", dir ? stepsToDo : -stepsToDo);
      wait_ms(20);
      while (!feedback)
      {
        while (((distFL <= stopDist && dir) ||
             (distFR <= stopDist && dir) ||
             (distBL <= stopDist && !dir) ||
             (distBR <= stopDist && !dir)))
        {
          interruptDrive = 1;
          printf("Obstacle: %d, %d, %d, %d\n", distFL, distFR, distBL, distBR);
        }
        interruptDrive = 0;
      }
    }
  }
  else
  {
    printf("I will do %d steps without CoA\n", stepsToDo);
    while (!feedback)
    {
      wait_ms(5);
    }

    if (feedback)
    {
      driveSerial.printf("!!t%5d\n", dir ? stepsToDo : -stepsToDo);
      wait_ms(stepsToDo * tPerStep / 1000 / 2);
      while (!feedback)
      {
        wait_ms(5);
      }
    }
  }

  return stepsDone * anglePerStep;
}

/**
 * Method to turn the bot
 * @param distance  Distance which the bot has to drive. Measured in mm
 * @param dir       Direction in wich the bot will turn true: forward | false: backward
 * @param useCoA    Wether to use collision avoidance or not
*/
float drive(float distance, bool dir, bool useCoA = true)
{
  int stepsDone = 0;
  int stepsToDo = distance / distancePerStep;
  if (useCoA)
  {
    printf("I will do %d steps with CoA\n", stepsToDo);
    while (!feedback)
    {
      wait_ms(5);
    }

    if (feedback)
    {
      driveSerial.printf("!!d%5d\n", dir ? stepsToDo : -stepsToDo);
      wait_ms(20);
      while (!feedback)
      {
        while (((distFL <= stopDist && dir) ||
             (distFR <= stopDist && dir) ||
             (distBL <= stopDist && !dir) ||
             (distBR <= stopDist && !dir)))
        {
          interruptDrive = 1;
          printf("Obstacle: %d, %d, %d, %d\n", distFL, distFR, distBL, distBR);
        }
        interruptDrive = 0;
      }
    }
  }
  else
  {
    printf("I will do %d steps without CoA\n", stepsToDo);
    while (!feedback)
    {
      wait_ms(5);
    }

    if (feedback)
    {
      driveSerial.printf("!!d%5d\n", dir ? stepsToDo : -stepsToDo);

      wait_ms(stepsToDo * tPerStep / 1000 / 2);
      while (!feedback)
      {
        wait_ms(5);
      }
    }
  }

  return stepsDone * distancePerStep;
}

/**
 * Method to turn the bot
 * @param distance  Distance which the bot has to drive. Measured in mm
 * @param dir       Direction in wich the bot will turn true: forward | false: backward
 * 
 * @returns float   Time in wich the drive will be done
*/
float startDrive(float distance, bool dir)
{
  float timeItWillTake = 0;
  unsigned int stepsToDo = distance / distancePerStep;
  float a = 0;
  for (unsigned int i = 0; i < stepsToDo; i++)
  {
    a = 0;
    if (i < 400 && i <= stepsToDo / 2)
    {
      a = tPerStep * 0.0025 * (400 - i);
    }

    if (stepsToDo - i < 400 && i > stepsToDo / 2)
    {
      a = tPerStep * 0.0025 * (400 - (stepsToDo - i));
    }
    
    timeItWillTake += (tPerStep + a) * 2;
  }
  timeItWillTake /= 1000;
  timeItWillTake += 5;
  printf("I will do %d steps without CoA. This will take %f ms \n", stepsToDo, timeItWillTake);
  while (!feedback) // Wait until processor is ready
  {
    wait_ms(5);
  }

  if (feedback) // Only if processor is really ready
  {
    driveSerial.printf("!!d%d\n", dir ? stepsToDo : -stepsToDo);
  }

  while (!feedback) // Wait until movement is started
  {
    wait_us(5); // Only wait short times
  }
  wait_ms(15);
  return timeItWillTake;
}

float calcSlowDriveTime(unsigned int stepsToDo) {
  float timeItWillTake = 0;

  float a = 0;
  for (unsigned int i = 0; i < stepsToDo; i++)
  {
    a = 0;
    if (i < 400 && i <= stepsToDo / 2)
    {
      a = tPerSlowStep * 0.0025 * (400 - i);
    }

    if (stepsToDo - i < 400 && i > stepsToDo / 2)
    {
      a = tPerSlowStep * 0.0025 * (400 - (stepsToDo - i));
    }
    
    timeItWillTake += (tPerSlowStep + a) * 2;
  }
  timeItWillTake /= 1000.0;
  timeItWillTake += 5.0;

  return timeItWillTake;
}

/**
 * Method to turn the bot
 * @param distance  Distance which the bot has to drive. Measured in mm
 * @param dir       Direction in wich the bot will turn true: forward | false: backward
 * 
 * @returns float   Time in wich the drive will be done
*/
float startSlowDrive(float distance, bool dir)
{
  float timeItWillTake = 0;
  unsigned int stepsToDo = distance / distancePerStep;

  timeItWillTake = calcSlowDriveTime(stepsToDo);
  
  printf("I will do %d steps without CoA. This will take %f ms \n", stepsToDo, timeItWillTake);
  while (!feedback) // Wait until processor is ready
  {
    wait_ms(5);
  }

  if (feedback) // Only if processor is really ready
  {
    driveSerial.printf("!!s%d\n", dir ? stepsToDo : -stepsToDo);
  }

  while (!feedback) // Wait until movement is started
  {
    wait_us(5); // Only wait short times
  }
  wait_ms(5);
  return timeItWillTake;
}

#endif
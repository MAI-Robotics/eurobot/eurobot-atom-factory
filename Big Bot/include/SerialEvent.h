#include "mbed.h"
#ifndef SERIALEVENT_H
#define SERIALEVENT_H
 
const uint8_t STRMAX = 20;  
const char EOT = '\n';
const char CRLF = '\n';
// ---------------- Serial RS232 Event Class  --------------------------
class SerialEvent {
        Serial _pc;
        void _risingISR();
        char _str[STRMAX]; 
        volatile bool _strOkFlag;
        int _index;
 
 
    public:
        SerialEvent(PinName tx, PinName rx) : _pc(tx, rx) { // create the Serial on the pin specified to SwEvent
            _pc.attach(callback(this, &SerialEvent::pc_recv));        // attach DataReceive-function of this SerialEvent instance 
            _strOkFlag = false;
            _index=0;
 
        }
        void baud(int baud);
        void pc_recv();
        void getString(char st[]);
        int checkFlag();                                    // must in do-condition (while(true)-loop) continuously interrogated
};
// ---------------- Serial Event Class Methodes --------------------------
void SerialEvent::getString(char st[]) {
    for( int i=0; i <= _index; i++)
        st[i] = _str[i];
    _index=0;
}

void SerialEvent::baud(int baud) {
    _pc.baud(baud);
}
 
void SerialEvent::pc_recv() {
    char c;
    while(_pc.readable()){
        c = _pc.getc();
        if((c != CRLF) && (_index < STRMAX)) {
            _str[_index++] = c;
        }
    }
    if(( c == EOT)) {           // end: . string not empty
        if(_index >= 1) {
            _strOkFlag = true;
            _str[--_index] = 0; 
        }
    }
}
 
int SerialEvent::checkFlag() {
    if( _strOkFlag ) {
        _strOkFlag = false; 
        return 1;
    }
    return 0;
}
#endif